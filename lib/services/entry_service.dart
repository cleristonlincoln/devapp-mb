import 'dart:convert';

import 'package:devapp/core/model/entry_resume_model.dart';
import 'package:devapp/services/http_intercepror.dart';
import 'package:devapp/services/response_entry_resume.dart';
import 'package:devapp/utils/shared_preferences_utils.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';

class EntryService {
  http.Client cliente =
      InterceptedClient.build(interceptors: [HttpInterceptor()]);

  static const String url =
      'http://devlcode-app-prd.eba-mwtpfrmz.us-east-1.elasticbeanstalk.com/develcode-backend/entry/entry-report?pageFilter=1&sizeFilter=10&idUser=70&dtInicial=2023-04-01T03%3A00%3A00&dtFinal=2023-04-30T03%3A00%3A00';

  Future<ResponseEntryResume> getEntryResume() async {
    String token = await SharedPreferencesUtils.getString('token');

    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    };

    var response = await cliente.get(Uri.parse(url), headers: headers);

    Map<String, dynamic> mapResponse = jsonDecode(response.body);

    String soma = mapResponse['totalHours'];
    List resume = mapResponse['resultList'];

    List<EntryResumeModel> ok = resume
        .map<EntryResumeModel>((m) => EntryResumeModel.fromJson(m))
        .toList();

    return ResponseEntryResume(soma, ok);
  }
}
