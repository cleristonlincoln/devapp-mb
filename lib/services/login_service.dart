import 'dart:convert';

import 'package:devapp/core/model/login.dart';
import 'package:devapp/core/model/user_login.dart';
import 'package:http/http.dart' as http;
import 'package:http_interceptor/http_interceptor.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'http_intercepror.dart';

class LoginService {
  http.Client cliente =
      InterceptedClient.build(interceptors: [HttpInterceptor()]);

  // https://64349af183a30bc9ad4e0c95.mockapi.io/api/v1/pessoa
  static const String url = 'http://172.29.160.1:8080/develcode-backend';

  Future<http.Response> login(Login login) async {
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      //  'Authorization': '<Your token>'
    };

    String bode = jsonEncode(login.toJson());

    var response = await cliente.post(Uri.parse('$url/authentication'),
        body: bode, headers: requestHeaders);

    Map<String, dynamic> mapResponse = jsonDecode(response.body)["data"];

    String token = mapResponse["dsToken"].toString().replaceAll("Bearer ", "");

    int id = mapResponse["id"];
    String email = mapResponse["desEmail"];
    bool? active = mapResponse["indInativo"];
    int idOffice = mapResponse["office"]["id"];
    var dados = UserLogin(id, email, token, active, idOffice);

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt("id", dados.id);
    sharedPreferences.setString("email", dados.email);
    sharedPreferences.setInt("office", dados.idOffice);
    sharedPreferences.setString("token", dados.token);
    return response;
  }

  Future<http.Response> recuperarEmail(String email) async {
    return await cliente.get(Uri.parse('$url/user/reset-pass-email/$email'));
  }
}

class UserNotFoundException implements Exception {}
