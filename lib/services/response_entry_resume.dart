import '../core/model/entry_resume_model.dart';

class ResponseEntryResume {
  late String totalHours;
  late List<EntryResumeModel> entries;

  ResponseEntryResume(this.totalHours, this.entries);
}
