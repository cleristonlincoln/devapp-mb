
import 'package:http_interceptor/http/interceptor_contract.dart';
import 'package:http_interceptor/models/request_data.dart';
import 'package:http_interceptor/models/response_data.dart';
import 'package:logger/logger.dart';

class HttpInterceptor implements InterceptorContract {

  Logger log = Logger();

  @override
  Future<RequestData> interceptRequest({required RequestData data}) async {
    log.v(
        'URL: ${data.method}${data.baseUrl} \nheaders: ${data
            .headers} \nparams: ${data.params} \nbody: ${data.body} \n');
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({required ResponseData data}) async {
    log.v(
        'URL: ${data.method}${data.url} \nheaders: ${data
            .headers} \nstatusCode: ${data.statusCode} \nbody: ${data.body} \n');
    return data;
  }
}
