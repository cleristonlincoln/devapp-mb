import 'package:flutter/material.dart';

class EntryResumeCard extends StatefulWidget {
  final String dayString;
  final String date;
  final String hours;

  const EntryResumeCard(
      {Key? key,
      required this.dayString,
      required this.date,
      required this.hours})
      : super(key: key);

  @override
  State<EntryResumeCard> createState() => _EntryResumeCardState();
}

class _EntryResumeCardState extends State<EntryResumeCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.blueAccent),
                  height: 72,
                  child: InkWell(
                    onTap: () {
                      print('toque no card');
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        children: [
                          Text(
                            widget.dayString,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          const Expanded(
                            // <-- SEE HERE
                            child: SizedBox.shrink(),
                          ),
                          Text(
                            widget.date,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          const Expanded(
                            // <-- SEE HERE
                            child: SizedBox.shrink(),
                          ),
                          Text(
                            widget.hours,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ))
            ],
          ),
        )
      ],
    );
  }
}
