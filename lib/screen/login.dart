import 'package:flutter/material.dart';

import '../core/model/login.dart';
import '../services/login_service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  LoginService service = LoginService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(0, 150, 0, 20),
                alignment: Alignment.topCenter,
                child: Image.asset('assets/images/logo-devel.png'),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 64, 0, 43),
                child: TextFormField(
                  controller: email,
                  decoration: const InputDecoration(label: Text('E-mail')),
                ),
              ),
              Container(
                child: TextFormField(
                  controller: password,
                  obscureText: true,
                  decoration: const InputDecoration(label: Text('Senha')),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(0, 32, 0, 0),
                child: ElevatedButton(
                  child: const Text('Entrar'),
                  onPressed: () async {
                    var login = Login(
                      desEmail: email.text,
                      desSenha: password.text,
                    );

                    service.login(login).then((value) => {
                          if (value.statusCode == 200)
                            {Navigator.pushReplacementNamed(context, 'segunda')}
                        });
                  },
                ),
              ),
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: TextButton(
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        "recuperar-senha",
                      );
                    },
                    child: Text('Esqueceu sua senha?'),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
