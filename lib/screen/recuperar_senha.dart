import 'package:devapp/services/login_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RecuperarSenhaScreen extends StatefulWidget {
  const RecuperarSenhaScreen({Key? key}) : super(key: key);

  @override
  State<RecuperarSenhaScreen> createState() => _RecuperarSenhaScreenState();
}

class _RecuperarSenhaScreenState extends State<RecuperarSenhaScreen> {
  TextEditingController email = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final formValid = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Recuperar senha.'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(8, 200, 0, 8),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: email,
                decoration: const InputDecoration(
                  label: Text(
                    'Email',
                  ),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Email é obrigatório';
                  }
                  return null;
                },
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 64, 0, 0),
              child: ValueListenableBuilder<bool>(
                valueListenable: formValid,
                builder: (_, formValid, child) {
                  return ElevatedButton(
                    onPressed: formValid
                        ? null
                        : () {
                            LoginService service = LoginService();

                            service.recuperarEmail(email.text).then((value) {
                              if (value.statusCode == 201) {
                                Navigator.pop(context);
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Email não encontrado",
                                    toastLength: Toast.LENGTH_LONG,
                                    gravity: ToastGravity.TOP,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: Colors.red,
                                    textColor: Colors.white,
                                    fontSize: 16.0);
                              }
                            });
                          },
                    child: const Text('Recuperar'),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
