import 'package:devapp/core/model/entry_resume_model.dart';
import 'package:devapp/services/entry_service.dart';
import 'package:devapp/services/response_entry_resume.dart';
import 'package:flutter/material.dart';

import '../components/entry_resume_card.dart';
import '../services/response_entry_resume.dart';
import '../shared/drawer.dart';

const List<String> list = <String>['One', 'Two', 'Three', 'Four'];

class EntryResumeScreen extends StatefulWidget {
  const EntryResumeScreen({Key? key}) : super(key: key);

  @override
  State<EntryResumeScreen> createState() => _EntryResumeScreenState();
}

class _EntryResumeScreenState extends State<EntryResumeScreen> {
  Future<ResponseEntryResume> list = getEntryResumeList();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const DrawerMenu(),
      appBar: AppBar(
        title: const Text('Consolidado de horas'),
      ),
      body: FutureBuilder(
          future: list,
          builder: (context, snapshot) {
            return ListView.builder(
                itemCount:snapshot.data?.entries.length,
                itemBuilder: (context, index) {
                  EntryResumeModel rm = snapshot.data!.entries[index];
                  return  EntryResumeCard(
                    date: rm.data,
                    dayString: rm.dayWeek,
                    hours: rm.hours,
                  );
                });
          }),
    );
  }
}

Future<ResponseEntryResume> getEntryResumeList() async {
  EntryService service = EntryService();
  ResponseEntryResume ed =  await service.getEntryResume();
  return ed;
}
