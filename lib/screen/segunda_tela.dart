import 'package:devapp/shared/drawer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SegundaTela extends StatefulWidget {
  const SegundaTela({Key? key}) : super(key: key);

  @override
  State<SegundaTela> createState() => _SegundaTelaState();
}

class _SegundaTelaState extends State<SegundaTela> {
  @override
  Widget build(BuildContext context) {
    Future<String> token = bucarUduario();

    return Scaffold(
      backgroundColor: Colors.lightGreenAccent,
      appBar: AppBar(
        title: const Text('edede'),
      ),
      drawer: const DrawerMenu(),
      body: FutureBuilder<String>(
        future: token,
        builder: (context, snapshot) {
          String? texto = snapshot.data;
          return Text(texto!);
        },
      ),
    );
  }

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token');
    if (token != null) {
      return token;
    }
    return '';
  }

  Future<String> bucarUduario() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('email');
    if (token != null) {
      return token;
    }
    return '';
  }
}
