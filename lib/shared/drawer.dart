import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerMenu extends StatefulWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  State<DrawerMenu> createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {
  Future<String> email = getEmailLogger();
  Future<String> office = getOfficeLogger();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: FutureBuilder<String>(
              future: office,
              builder: (context, snapshot) {
                String? textOffice = snapshot.data;
                return Text(textOffice!);
              },
            ),
            accountEmail: FutureBuilder<String>(
              future: email,
              builder: (context, snapshot) {
                String? textUser = snapshot.data;
                return Text(textUser!);
              },
            ),
            currentAccountPicture: const CircleAvatar(
              backgroundImage: NetworkImage(
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTzeWvDSqsx7HoZa1WPaWArZ4X95zd2tFgZQ&usqp=CAU'),
            ),
          ),
          ListTile(
            title: const Text('Consolidado'),
            onTap: () =>
                Navigator.pushReplacementNamed(context, "entry-service"),
          ),
          ListTile(
            title: const Text('Sair'),
            onTap: () {
              logout();
            },
          )
        ],
      ),
    );
  }

  logout() {
    SharedPreferences.getInstance().then((value) {
      value.clear();
      Navigator.pushReplacementNamed(context, "login");
    });
  }

  static Future<String> getEmailLogger() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? email = preferences.getString('email');
    return email ?? 'n/d';
  }

  static Future<String> getOfficeLogger() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
      String? office = preferences.getString('officeName');
    return office ?? 'n/z';
  }
}
