class EntryResumeModel {
  late String data;
  late String dayWeek;
  late String hours;

  EntryResumeModel(
      {required this.data, required this.dayWeek, required this.hours});

  EntryResumeModel.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    dayWeek = json['dayWeek'];
    hours = json['hours'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['data'] = this.data;
    data['dayWeek'] = this.dayWeek;
    data['hours'] = this.hours;
    return data;
  }
}
