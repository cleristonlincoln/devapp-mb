class UserLogin {
  int _id;
  String _email;
  String _token;
  bool? _active;
  int _idOffice;

  UserLogin(this._id, this._email, this._token, this._active, this._idOffice);

  int get idOffice => _idOffice;

  set idOffice(int value) {
    _idOffice = value;
  }


  set active(bool value) {
    _active = value;
  }

  String get token => _token;

  set token(String value) {
    _token = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }
}
