class Login{
  String desEmail;
  String desSenha;

  Login({required this.desEmail, required this.desSenha});

  Map<String, dynamic> toJson() =>
      {
        'desEmail': desEmail,
        'desSenha': desSenha,
      };

}
