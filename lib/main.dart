import 'package:devapp/screen/entry-resume.dart';
import 'package:devapp/screen/login.dart';
import 'package:devapp/screen/recuperar_senha.dart';
import 'package:devapp/screen/segunda_tela.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  bool isLogged = await verifyToken();

  runApp(DevApp(
    isLogged: isLogged,
  ));
}

verifyToken() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  return sharedPreferences.get("token") != null;
}

class DevApp extends StatefulWidget {
  final bool isLogged;

  const DevApp({Key? key, required this.isLogged}) : super(key: key);

  @override
  State<DevApp> createState() => _DevAppState();
}

class _DevAppState extends State<DevApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: FToastBuilder(),
      navigatorKey: navigatorKey,
      initialRoute: widget.isLogged ? "entry-service" : "login",
      routes: {
        "login": (context) => const LoginScreen(),
        "segunda": (context) => const SegundaTela(),
        "recuperar-senha": (context) => const RecuperarSenhaScreen(),
        "entry-service": (context) => const EntryResumeScreen(),
      },
    );
  }
}
